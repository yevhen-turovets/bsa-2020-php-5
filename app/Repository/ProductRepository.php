<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Illuminate\Support\Arr;

class ProductRepository implements ProductRepositoryInterface
{
    private $products;

	public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function findAll(): array
    {
        return $this->products;
    }

    public function getMostPopularProduct(): Product
    {
        usort($this->products, array($this, "sortProductByPopular"));

        return Arr::first($this->products);
    }

    public function getCheapestProducts($count): array
    {
        usort($this->products, array($this, "sortProductByCheapest"));
        $products =  array_slice($this->products, 0, $count);
        arsort($products);

        return $products;
    }

    private function sortProductByPopular($a, $b)
    {
        return $a->getRating() < $b->getRating();
    }

    private function sortProductByCheapest($a, $b)
    {
        return $a->getPrice() > $b->getPrice();
    }
}
