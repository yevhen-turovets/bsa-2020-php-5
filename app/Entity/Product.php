<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * Class Tweet
 * @package App\Entity
 * @property int $id
 * @property string $name
 * @property string $image_url
 * @property float $price
 * @property float $rating
 */
class Product
{
    private $id;
    private $name;
    private $image_url;
    private $price;
    private $rating;

    public function __construct($id, $name, $price, $image_url, $rating)
    {
        $this->id = $id;
        $this->name = $name;
        $this->image_url = $image_url;
        $this->price = $price;
        $this->rating = $rating;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getImageUrl(): string
    {
        return $this->image_url;
    }

    public function getRating(): float
    {
        return $this->rating;
    }
}
