<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
    <!-- Styles -->
    <style>
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            border-collapse: collapse;
            color: #686461;
        }
        caption {
            padding: 10px;
            color: white;
            background: #8FD4C1;
            font-size: 18px;
            text-align: left;
            font-weight: bold;
        }
        th {
            border-bottom: 3px solid #B9B29F;
            padding: 10px;
            text-align: left;
        }
        td {
            padding: 10px;
        }
        tr:nth-child(odd) {
            background: white;
        }
        tr:nth-child(even) {
            background: #E8E6D1;
        }
    </style>
   </head>
<body>
<div align="center">
    <table>
        <caption>Cheap products</caption>
        <tbody>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Price</td>
                <td>Image Url</td>
                <td>Rating</td>
            </tr>
            @foreach($productsArray as $product)
                <tr>
                    <td>{{ $product['id'] }}</td>
                    <td>{{ $product['name'] }}</td>
                    <td>{{ $product['price'] }}</td>
                    <td>{{ $product['img'] }}</td>
                    <td>{{ $product['rating'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
