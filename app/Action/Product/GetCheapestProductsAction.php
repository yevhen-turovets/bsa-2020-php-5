<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private ProductRepositoryInterface $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetCheapestProductsResponse
    {
        $products = $this->repository->getCheapestProducts(3);

        return new GetCheapestProductsResponse($products);
    }
}
