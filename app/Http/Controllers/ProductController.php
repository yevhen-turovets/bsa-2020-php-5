<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

class ProductController extends Controller
{
    private $getAllProductsAction;
    private $getMostPopularProductAction;
    private $getCheapestProductsAction;
    private $presenter;

    public function __construct(
        GetAllProductsAction $getAllProductsAction,
        GetMostPopularProductAction $getMostPopularProductAction,
        GetCheapestProductsAction $getCheapestProductsAction,
        ProductArrayPresenter $presenter
    )
    {
        $this->getAllProductsAction = $getAllProductsAction;
        $this->getMostPopularProductAction = $getMostPopularProductAction;
        $this->getCheapestProductsAction = $getCheapestProductsAction;
        $this->presenter = $presenter;
    }

    public function getProductCollection()
    {
        $products = $this->getAllProductsAction->execute()->getProducts();

        return response()->json($this->presenter::presentCollection($products));
    }

    public function getMostPopularProduct()
    {
        $product = $this->getMostPopularProductAction->execute()->getProduct();

        return response()->json($this->presenter::present($product));
    }

    public function getCheapestProducts()
    {
        $products = $this->getCheapestProductsAction->execute()->getProducts();
        $productsArray = $this->presenter::presentCollection($products);

        return view('cheap_products', compact('productsArray'));
    }
}
