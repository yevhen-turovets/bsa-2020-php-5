<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    private ProductRepositoryInterface $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $product = $this->repository->getMostPopularProduct();

        return new GetMostPopularProductResponse($product);
    }
}
