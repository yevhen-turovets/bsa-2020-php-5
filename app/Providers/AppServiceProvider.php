<?php

namespace App\Providers;

use App\Repository\ProductRepository;
use Illuminate\Support\ServiceProvider;
use App\Repository\ProductRepositoryInterface;
use App\Services\ProductGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductRepositoryInterface::class, function () {
            return new ProductRepository(ProductGenerator::generate());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
